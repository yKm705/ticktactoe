class GameBoard:

    def __init__(self, board_size):
        self.game_mode = board_size
        self.clean_board()

    def clean_board(self):
        self.matrix = [
                [' ' for i in range(self.game_mode)]
                for j in range(self.game_mode)
                        ]

    def printBoard(self):
        print("Current Board")
        print()
        print(
                str(self.matrix[0][0]) + " | " + str(self.matrix[0][1]) +
                " | " + str(self.matrix[0][2])
                )
        print(
                str(self.matrix[1][0]) + " | " + str(self.matrix[1][1]) +
                " | " + str(self.matrix[1][2])
                )
        print(
                str(self.matrix[2][0]) + " | " + str(self.matrix[2][1]) +
                " | " + str(self.matrix[2][2])
                )
        print()

    def checkPosition(self, b, a, player_piece):
        try:
            b = int(b) - 1
            a = int(a) - 1

            if(a >= 0 and b >= 0):
                if(a < self.game_mode and b < self.game_mode):
                    validInput = True
                    if(self.matrix[a][b] != ' '):
                        print("Cannot play on this location")
                        validInput = False

                    return validInput
            else:
                print("Incorrect coordinates: Try again")

        except ValueError:
            print("An invalid value was entered!!")
            print("Oops,There goes your turn!! \
                    (Be more careful with your entries next turn)")
            input("Press any button to continue")

    def setPosition(self, b, a, player_piece):
        self.matrix[a][b] = player_piece

    def winCheck(self, player):
        """Function checks whether the game has been won"""

        for i in range(0, self.game_mode):
            if(self.matrix[i][0] == player and self.matrix[i][1] == player and
                    self.matrix[i][2] == player):
                    return True

        for j in range(0, self.game_mode):
            if(self.matrix[0][j] == player and self.matrix[1][j] == player and
                    self.matrix[2][j] == player):
                    return True

        if(self.matrix[0][0] == player and self.matrix[1][1] == player and
                self.matrix[2][2] == player):
            return True

        if(self.matrix[0][2] == player and self.matrix[1][1] == player and
                self.matrix[2][0] == player):
            return True

    def checkFull(self):
        freeSpaces = 0

        for i in range(0, self.game_mode):
            for j in range(0, self.game_mode):
                print(self.matrix[i][j])
                if(self.matrix[i][j] == ' '):
                    freeSpaces += 1

        if(freeSpaces == 0):
            return True
        else:
            return False

    def cleanSlate(self):
        self.clean_board()

        global player1
        global player2

        player1 = 'player1'
        player2 = 'player2'
