#!/usr/bin/python3

import os
import sys

"""Initial game setup"""
"""
If game_mode is to be adjustable printBoard function should probably take it
as an argument
"""
os.system("clear")
game_mode = 3
Matrix = [[' ' for i in range(0, game_mode)] for j in range(0, game_mode)]

player1 = 'player1'
player2 = 'player2'


def cleanSlate():
    for i in range(0, game_mode):
        for j in range(0, game_mode):
            Matrix[i][j] = ' '

    global player1
    global player2

    player1 = 'player1'
    player2 = 'player2'


def printBoard():
    print("Current Board")
    print()
    print(
            str(Matrix[0][0]) + " | " + str(Matrix[0][1]) + " | " +
            str(Matrix[0][2])
            )
    print(
            str(Matrix[1][0]) + " | " + str(Matrix[1][1]) + " | " +
            str(Matrix[1][2])
            )
    print(
            str(Matrix[2][0]) + " | " + str(Matrix[2][1]) + " | " +
            str(Matrix[2][2])
            )
    print()


def setupBoard():

    global player1
    global player2

    while(player1 != 'x' and player1 != 'o'):
        player1 = input("Player 1, please choose x or o\n")
        os.system("clear")

    print("Players selected!!")

    if(player1 == 'x'):
        print("Player 1 is \'x\' and player 2 is \'o\'")
        player2 = 'o'
    else:
        print("Player 1 is \'o\' and player 2 is \'x\'")
        player2 = 'x'


def move(player):

    validInput = False
    try:
        while(validInput is False):

            b, a = input("Please enter the coordinates\teg: 2,1\n").split(',')

            b = int(b) - 1
            a = int(a) - 1

            if(a >= 0 and b >= 0):
                if(a < game_mode and b < game_mode):
                    validInput = True
                    if(Matrix[a][b] != ' '):
                        print("Cannot play on this location")
                        validInput = False
            else:
                print("Incorrect coordinates: Try again")

        Matrix[a][b] = player
        winCheck(player)
    except ValueError:
        print("An invalid value was entered!!")
        print("Oops,There goes your turn!! \
                (Be more careful with your entries next turn)")
        input("Press any button to continue")


def winCheck(player):
    """Function checks whether the game has been won"""

    for i in range(0, game_mode):
        if(Matrix[i][0] == player and Matrix[i][1] == player and
                Matrix[i][2] == player):
                return True

    for j in range(0, game_mode):
        if(Matrix[0][j] == player and Matrix[1][j] == player and
                Matrix[2][j] == player):
                return True

    if(Matrix[0][0] == player and Matrix[1][1] == player and
            Matrix[2][2] == player):
        return True

    if(Matrix[0][2] == player and Matrix[1][1] == player and
            Matrix[2][0] == player):
        return True


def checkFull():
    freeSpaces = 0

    for i in range(0, game_mode):
        for j in range(0, game_mode):
            print(Matrix[i][j])
            if(Matrix[i][j] == ' '):
                freeSpaces += 1

    if(freeSpaces == 0):
        return True
    else:
        return False


def swapPlayer(player):
    """Simply swaps the current player to the next"""
    if(player == 'x'):
        return 'o'
    elif(player == 'o'):
        return 'x'
    else:
        print("Error has occured")
        sys.exit(0)


def main():

    try:
        playAgain = True

        while(playAgain is True):
            setupBoard()

            finished = False    # Change to False
            won = False

            currentPlayer = player1
            while(finished is False):

                print()
                print("Player Turn: " + currentPlayer)
                printBoard()
                move(currentPlayer)
                won = winCheck(currentPlayer)
                if(won is True):
                    finished = True
                else:
                    finished = checkFull()

                currentPlayer = swapPlayer(currentPlayer)
                os.system("clear")

            printBoard()

            if(won is True):
                print("Congratulations!!!!")
                currentPlayer = swapPlayer(currentPlayer)
                print("The player : " + currentPlayer + " is the winner!!")
            else:
                print("Game finished but no-one won")

            replay = input("Do you want to play again?\t(\'Y\' or \'y\')\n")
            if(replay != 'y' and replay != 'Y'):
                playAgain = False
            else:
                cleanSlate()
                # _ = os.system("clear")
                os.system("clear")

    except KeyboardInterrupt:
        print("\nClosing Program...")


if __name__ == '__main__':
    main()
