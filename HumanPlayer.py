from Player import Player


class HumanPlayer(Player):
    print("Human player selected.")

    def __init__(self):
        super(Player, self)

    def move(self):

        validInput = False
        while(validInput is False):

            b, a = input("Please enter the coordinates\teg: 2,1\n").split(',')
            validInput = self.game_board.checkPosition(b, a, self.game_piece)

        b = int(b) - 1
        a = int(a) - 1

        self.game_board.setPosition(b, a, self.game_piece)
