#!/usr/bin/python3

import os
from HumanPlayer import HumanPlayer
from AI import AI
from GameBoard import GameBoard

"""Initial game setup"""
"""
If game_mode is to be adjustable printBoard function should probably take it
as an argument
"""


def main():
    os.system('clear')
    try:

        g_board = GameBoard(3)
        g_board.cleanSlate()
        player1, player2 = determine_players()

        player1.setName('Player1')
        player1.setBoard(g_board)
        player1.setPiece('O')

        player2.setName('Player2')
        player2.setBoard(g_board)
        player2.setPiece('X')

        current_player = player1

        while(True):
            os.system('clear')
            print("Player: {} turn. Player piece: {}".format(
                current_player.name, current_player.game_piece)
                )
            g_board.printBoard()
            current_player.move()

            if g_board.winCheck(current_player.game_piece):
                print("Game has been won by: {}.".format(current_player.name))
                break

            if g_board.checkFull():
                print("Game has come to a tie.")
                break

            current_player = change_player(current_player, player1, player2)

    except KeyboardInterrupt:
        print("\nClosing Program...")


def determine_players():

    player1 = determine_player()
    player2 = determine_player()

    return player1, player2


def determine_player():
    while(True):
        user_input = input("Please enter the player type.\
                \n\t[ H - human ]\n\t[ A - AI ]")
        if user_input == 'H':
            return HumanPlayer()

        elif user_input == 'A':
            return AI()

        else:
            print("Correct input. Try again.")


def change_player(current_player, player1, player2):
    if current_player == player1:
        return player2
    else:
        return player1


if __name__ == '__main__':
    main()
